package restrictions;

public class Main {
    public static void main(String[] args) {
        Account[] accounts = new Account[]{
               new Account(1),
               new Account(2),
               new Account(3)
        };

        Bank<IAccount> bank = new Bank<>(accounts);
        bank.accountsInfo();
    }
}
