package restrictions;

public class Bank<T extends IAccount> {
    private T[] accounts;

    public Bank(T[] accounts) {
        this.accounts = accounts;
    }

    public void accountsInfo(){
        for(IAccount account: accounts){
            System.out.println(account.getId());
        }
    }
}
