package inheritance2;

public class DepositAccount<T, S> extends Account<T> {
    private S name;

    S getName(){
        return name;
    }

    public DepositAccount(T id, S name) {
        super(id);
        this.name = name;
    }
}
