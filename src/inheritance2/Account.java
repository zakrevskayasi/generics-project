package inheritance2;

public class Account<T> {
    private T id;

    T getId(){
        return id;
    }

    public Account(T id) {
        this.id = id;
    }
}
