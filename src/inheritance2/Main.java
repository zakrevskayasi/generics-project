package inheritance2;

import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        DepositAccount<Integer, String> depositAccount1 = new DepositAccount<>(20, "Tom");
        System.out.println(depositAccount1.getId() + ":" + depositAccount1.getName());

        DepositAccount<String, Integer> depositAccount2 = new DepositAccount<>("12345", 12345);
        System.out.println(depositAccount2.getId() + ":" + depositAccount2.getName());


    }
}
