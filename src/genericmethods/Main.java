package genericmethods;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<>();
        intList.add(1);
        intList.add(2);
        intList.add(3);
        System.out.println("List before Generic processing: " + intList);

        Utilities.fill(intList, 0);
        System.out.println("List after Generic processing: " + intList);
    }
}
