package liambda;

@FunctionalInterface
public interface Operationable {
    int calculate(int x, int y);
}
