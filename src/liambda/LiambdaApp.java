package liambda;

public class LiambdaApp {
    public static void main(String[] args) {
        /*Operationable operation;
        operation = (x, y) -> x + y;

        System.out.println(operation.calculate(5, 10));*/

        Operationable operationable = new Operationable() {
            @Override
            public int calculate(int x, int y) {
                return x + y;
            }
        };

        System.out.println(operationable.calculate(5, 10));

    }
}
