package constructors;

public class Operation {
    double x1;
    double x2;

    <T extends Number> Operation(T d1, T d2){
        this.x1 = d1.doubleValue();
        this.x2 = d2.doubleValue();
    }

    double getSum(){
        return x1 + x2;
    }
}
