package interfaces;

public class Main {
    public static void main(String[] args) {
        Operation op = new Operation(new Account(21));
        IAccount account = op.getAccount();
        System.out.println(account.getId());
    }
}
