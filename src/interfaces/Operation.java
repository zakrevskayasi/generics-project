package interfaces;

public class Operation implements Accountable<IAccount> {
    IAccount account;

    public Operation(IAccount account) {
        this.account = account;
    }

    @Override
    public IAccount getAccount() {
        return account;
    }
}
