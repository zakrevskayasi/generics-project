package interfaces;

public interface Accountable<T> {
    T getAccount();
}
