package interfaces;

public class Account implements IAccount {
    private int _id;

    public Account(int _id) {
        this._id = _id;
    }

    @Override
    public int getId() {
        return _id;
    }
}
