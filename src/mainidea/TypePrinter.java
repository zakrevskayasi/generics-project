package mainidea;

public class TypePrinter {
    private Object val;

    public Object getVal() {
        return val;
    }

    public TypePrinter(Object val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "mainidea.TypePrinter{" +
                "val=" + val +
                '}';
    }
}
