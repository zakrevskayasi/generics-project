package mainidea;

public class Main {

    public static void main(String[] args) {
        /*TypePrinter integerPrinter = new TypePrinter(new Integer(10));
        System.out.println(integerPrinter);
        Integer intValue = (Integer) integerPrinter.getVal();

        TypePrinter stringPrinter = new TypePrinter("Hello");
        System.out.println(stringPrinter);

        //Программист допустил ошибку
        Integer value = (Integer) stringPrinter.getVal();
        System.out.println(value);*/


        BoxPrinter<Integer> integerPrinter = new BoxPrinter<>(new Integer(10));
        System.out.println(integerPrinter);
        Integer value1 = integerPrinter.getVal();

        BoxPrinter<String> stringBoxPrinter = new BoxPrinter<>("Hello");
        System.out.println(stringBoxPrinter);

        //Повторим ошибку
        /*Integer value = stringBoxPrinter.getVal();
        System.out.println(value);*/
    }
}
