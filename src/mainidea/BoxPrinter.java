package mainidea;

public class BoxPrinter<T> {
    private T val;

    public T getVal() {
        return val;
    }

    public BoxPrinter(T val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "BoxPrinter{" +
                "val=" + val +
                '}';
    }
}
