package inheritance;

public class Main {
    public static void main(String[] args) {
        DepositAccount depositAccount = new DepositAccount(20);
        System.out.println(depositAccount.getId());

        DepositAccount depositAccount2 = new DepositAccount("12345");
        System.out.println(depositAccount2.getId());
    }
}
