package masks;

import java.util.ArrayList;
import java.util.List;

public class Main {
    static void printList(List<?> list){
        for(Object o: list){
            System.out.println("{" + o + "}");
        }
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(100);
        printList(list);

        List<String> strList = new ArrayList<>();
        strList.add("10");
        strList.add("100");
        printList(strList);

        //Ошибка
//        List<?> intList = new ArrayList<Integer>();
//        intList.add(new Integer(10));
    }
}
